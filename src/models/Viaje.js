const { Schema,model } = require('mongoose');

const ViajeSchema = new Schema ({
    valor: { type: Number, required: true },
	origen: { type: String, required: true },
	destino: { type: String, default: true },
	fecha: { type: String, required: true },
    horaSalida: { type: String, required: true },
    bus: {
		type: Schema.ObjectId, ref: "Bus"
	}
})

module.exports = model('Viaje', ViajeSchema);