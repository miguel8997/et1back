const { Schema, model } = require('mongoose');

const pasajeSchema = new Schema({
	origen: { type: String, required: true },
	destino: { type: String, required: true },
	fecha: { type: String, required: true },
	hora: { type: String, required: true },
	asiento: { type: Number, required: true },
	user: {
		type: Schema.ObjectId, ref: "User"
	}
}, {
	timestamps: true
})

module.exports = model('Pasaje', pasajeSchema);