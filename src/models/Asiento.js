const { Schema,model } = require('mongoose');

const AsientoSchema = new Schema ({
	numero: { type: Number, required: true },
	ocupado: { type: Boolean, default: false },
	avatar: { type: String, required: true },
    bus: {
		type: Schema.ObjectId, ref: "Bus"
	}
})

module.exports = model('Asiento', AsientoSchema);