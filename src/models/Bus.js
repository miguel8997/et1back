const { Schema, model } = require('mongoose');

const busSchema = new Schema({
	conductor: { type: String, required: true },
	patente: { type: String, required: true },
	asientos: [{
		type: Schema.ObjectId, ref: "Asiento"
	}]
}, {
	timestamps: true
})

module.exports = model('Bus', busSchema);