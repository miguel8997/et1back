const { Router } = require('express');
const router = Router();

const Role = require('../helpers/role');
const ViajeController = require('../controllers/ViajeController');
const verifyAuth = require('../middlewares/verifyAuth');

router.post('/createViajes', ViajeController.createViajes);
router.get('/getViaje/:origen&:destino&:fecha&:horaSalida',verifyAuth(), ViajeController.getViajeByPara);

module.exports = router;