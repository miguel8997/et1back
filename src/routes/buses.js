const { Router } = require('express');
const router = Router();

const Role = require('../helpers/role');
const BusController = require('../controllers/BusController');
const verifyAuth = require('../middlewares/verifyAuth');

router.get('/buses', verifyAuth([Role.Admin] || [Role.Mod]), BusController.getAllBuses);
router.post('/createBus', verifyAuth([Role.Admin] || [Role.Mod]), BusController.createBus);

module.exports = router;