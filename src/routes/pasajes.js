const { Router } = require('express');
const router = Router();

const Role = require('../helpers/role');
const PasajeController = require('../controllers/PasajeController');
const verifyAuth = require('../middlewares/verifyAuth');

router.get('/getUserPasajes/:id', PasajeController.getUserPasajes);
router.post('/createPasaje/:id', PasajeController.createPasaje);

module.exports = router;