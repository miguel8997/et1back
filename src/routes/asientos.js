const { Router } = require('express');
const router = Router();
const verifyAuth = require('../middlewares/verifyAuth');

const AsientoController = require('../controllers/AsientoController');

router.post('/createAsiento/:id', AsientoController.createAsiento);
router.put('/updateAsiento/:id', AsientoController.updateAsiento);

module.exports = router;