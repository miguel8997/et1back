const Bus = require('../models/Bus');
const Asiento = require('../models/Asiento');
const { busSchema } = require('../helpers/joi/schemaValidate');
const { fullURL } = require('../helpers/utils');


//agregar bus
// const createBus = async (req, res) => {
//     try {
//       const { error } = busSchema.validate(req.body);
//       if (error) return res.status(400).json({ error: error.details[0].message });

//       const isPatenteExist = await Bus.findOne({ patente: req.body.patente });
//       if (isPatenteExist) {
//         return res.status(400).json({ error: 'Patente ya registrada' })
//       }

//       const newBus = await Bus.create({ ...req.body});

//       for (let i = 0; i < 2; i++) {
//         const newAsiento =  await Asiento.create({
//            numero: (i+1), 
//            avatar: fullURL(req) + '/public/asiento/asiento_selec.png',
//            bus: newBus });
//       }

//       res.status(201).send(newBus);
//     } catch (error) {
//       res.status(500).send({ error });
//     }
// };

const createBus = async (req, res) => {
  try {
    // const { error } = busSchema.validate(req.body);
    // if (error) return res.status(400).json({ error: error.details[0].message });

    // const isPatenteExist = await Bus.findOne({ patente: req.body.patente });
    // if (isPatenteExist) {
    //   return res.status(400).json({ error: 'Patente ya registrada' })
    // }

    const newBus = new Bus({ ...req.body });

    for (let i = 0; i < 27; i++) {
      const newAsiento = new Asiento({
        numero: (i + 1),
        avatar: fullURL(req) + '/public/asiento/asiento_vacio.png'
      });
      newBus.asientos.push(newAsiento._id);
      newAsiento.bus = newBus;
      await newAsiento.save();
    }


    await newBus.save();

    res.status(201).send(newBus);
  } catch (error) {
    res.status(500).send({ error });
  }
};

async function createBusAuto(req,viaje) {
    const newBus = new Bus({
      conductor:'Pedro',
      patente:'12121212'
    });

    for (let i = 0; i < 27; i++) {
      const newAsiento = new Asiento({
        numero: (i + 1),
        avatar: fullURL(req) + '/public/asiento/asiento_vacio.png'
      });
      newBus.asientos.push(newAsiento._id);
      await newAsiento.save();
    }
    viaje.bus=newBus;
    await newBus.save();
    await viaje.save();
    
};

// Obtener todos los documentos
const getAllBuses = async (req, res) => {
  try {
    const buses = await Bus.find();
    res.json(buses);
  } catch (error) {
    return res.status(400).json({ error })
  }
};

module.exports = {
  createBus,
  getAllBuses,
  createBusAuto
};

