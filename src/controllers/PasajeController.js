const Pasaje = require('../models/Pasaje');

const createPasaje = async (req, res) => {
    try {
        
        const newPasaje = await Pasaje.create({ ...req.body, user: req.params.id });
        res.status(201).send(newPasaje);
    } catch (error) {
        res.status(500).send({ error });
    }
};

const getUserPasajes = async (req, res) => {
    try {
      const pasajes = await Pasaje.find({ user: req.params.id });
      res.json(pasajes);
    } catch (error) {
      return res.status(400).json({ error })
    }
  };

module.exports = {
    createPasaje,
    getUserPasajes
};