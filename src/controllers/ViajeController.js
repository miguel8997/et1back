const Viaje = require('../models/Viaje');
const BusController = require('../controllers/BusController');

const createViajes = async (req, res) => {
    try {

        const isViajeExist = await Viaje.findOne({
            origen: req.body.origen,
            destino: req.body.destino,
            fecha: req.body.fecha,
        });
        if (isViajeExist) {
            return res.status(400).json({ error: 'Este viaje ya se encuentra creado' })
        }

        const horas = ["08:30", "13:30", "16:45", "18:15", "21:15"];
        for (let i = 0; i < 5; i++) {
            const newViaje = await Viaje({ ...req.body });
            newViaje.valor = "5000";
            newViaje.horaSalida = horas[i];
            BusController.createBusAuto(req, newViaje);
        }
        res.status(201).send("nashe");
    } catch (error) {
        res.status(500).send({ error });
    }
};

const getViajeByPara = async (req, res) => {
    try {
        const viaje = await Viaje.findOne({
            origen: req.params.origen, destino: req.params.destino,
            fecha: req.params.fecha, horaSalida: req.params.horaSalida
        }).populate({
            path: 'bus',
            populate: { path: 'asientos' }
        }).exec();
        if (!viaje) return res.status(404).send({ error: 'Viaje no encontrado' })
        return res.send(viaje);
    } catch (viaje) {
        res.status(500).send({ error });
    }
};

module.exports = {
    createViajes,
    getViajeByPara
};