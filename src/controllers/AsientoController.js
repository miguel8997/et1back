const Asiento = require('../models/Asiento');

const createAsiento = async (req, res) => {
    try {
      const newAsiento = await Asiento.create({ ...req.body, bus: req.params.id });
      res.status(201).send(newAsiento);
    } catch (error) {
      res.status(500).send({ error });
    }
};

const updateAsiento = async (req, res) => {
  try {
    const asiento = await Asiento.findByIdAndUpdate(req.params.id, req.body, { new: true });
    res.json(asiento);
  } catch (error) {
    res.status(500).send({ error });
  }
};


module.exports = {
    createAsiento,
    updateAsiento,
};
